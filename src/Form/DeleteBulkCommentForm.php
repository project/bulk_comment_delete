<?php

namespace Drupal\bulk_comment_delete\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DeleteNodeForm.
 *
 * @package Drupal\batch_example\Form
 */
class DeleteBulkCommentForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_comment_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $query = \Drupal::database()->select('comment_field_data', 'comment')
      ->fields('node', ['type'])
      ->groupBy('node.type');
    $query->addExpression('COUNT(comment.cid)', 'cont');
    $query->leftJoin('node', 'node', 'node.nid = comment.entity_id');
    $results = $query->execute();
    while ($record = $results->fetchAssoc()) {
      $type = $record['type'];
      $content_type[$type] = $record['type'] . ' (' . $record['cont'] . ')';
    }
    if (isset($content_type)) {
      $form = [];
      $form['contypes'] = [
        '#type' => 'checkboxes',
        '#options' => $content_type,
        '#title' => $this->t('Delete comments according content type.'),
        '#required' => TRUE,
      ];
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Delete Comment'),
      ];
    }
    else {
      $form = [];
      $form['contact_information'] = [
        '#markup' => $this->t('No comment available.'),
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $content_types = $form_state->getValue('contypes');
    $_SESSION['content_type_value'] = $content_types;
    $form_state->setRedirect('bulk_comment_delete.multiple_delete_confirm');
  }

  /**
   * Resets the filter form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {
    $_SESSION['content_type_value'] = [];
  }

}
