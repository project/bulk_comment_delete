<?php

namespace Drupal\bulk_comment_delete\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class for confirm comment delete.
 */
class ConfirmCommentDelete extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'confirm_comment_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to remove commet of below content type.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('bulk_comment_delete.delete_comment');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (empty($_SESSION['content_type_value'])) {
      return $this->redirect('bulk_comment_delete.delete_comment');
    }
    foreach ($_SESSION['content_type_value'] as $types) {
      if (!empty($types)) {
        $value[] = $types;
      }
    }
    $form['comments'] = [
      '#theme' => 'item_list',
      '#items' => $value,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $content_types = $_SESSION['content_type_value'];
    $query = \Drupal::database()->select('comment_field_data', 'comment');
    $query->fields('comment', ['cid']);
    $query->leftJoin('node', 'node', 'node.nid = comment.entity_id');
    $query->condition('node.type', $content_types, 'IN');
    $result = $query->execute();
    foreach ($result as $rel) {
      $cids[] = $rel->cid;
    }
    $batch = [
      'title' => $this->t('Deleting Comment...'),
      'operations' => [
        [
          '\Drupal\bulk_comment_delete\BulkCommentDelete::bulkcommentdeletes',
          [$cids],
        ],
      ],
      'finished' => '\Drupal\bulk_comment_delete\BulkCommentDelete::bulkcommentdeletesFinishedCallback',
    ];
    batch_set($batch);
    $form_state->setRedirect('bulk_comment_delete.delete_comment');
  }

}
