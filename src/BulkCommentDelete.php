<?php

namespace Drupal\bulk_comment_delete;

/**
 * Class for comment delete.
 */
class BulkCommentDelete {

  /**
   * Implement function for delete comments batch.
   */
  public static function bulkcommentdeletes($cids, &$context) {
    $message = 'Deleting Comment...';
    $storage_handler = \Drupal::entityTypemanager()->getStorage("comment");
    $entities = $storage_handler->loadMultiple($cids);
    $storage_handler->delete($entities);
    $context['message'] = $message;
    $context['results'] = $cids;
  }

  /**
   * Batch Operation Callback.
   */
  public static function bulkcommentdeletesFinishedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results), 'One post processed.', '@count posts processed.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addStatus($message);
  }

}
