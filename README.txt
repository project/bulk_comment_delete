***********
* README: *
***********
CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
 ------------
The Bulk Comment Delete module provides functionality to delete all the comment
at once by selecting Node/Content type. One can find this inside the content
section from the admin menu.

* For a full description of the module, visit the project page:
   https://www.drupal.org/project/bulk_comment_delete

* To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/bulk_comment_delete

-- REQUIREMENTS --
No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.

CONFIGURATION
 -------------
After installation, navigate to Content > Comment Delete tab. From this page,
select which content types you would like to have comments deleted for. Click
Delete to process comment deletion.

MAINTAINERS
-----------
Current maintainers:
* Ved Pareek (vedpareek) - https://www.drupal.org/u/vedpareek
